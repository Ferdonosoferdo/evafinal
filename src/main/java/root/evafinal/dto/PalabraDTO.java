/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evafinal.dto;

/**
 *
 * @author ccruces
 */
public class PalabraDTO {

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
  private String palabra;
  private String significado;
  private String fecha;

 // Getter Methods 

  public String getPalabra() {
    return palabra;
  }

  public String getSignificado() {
    return significado;
  }

 // Setter Methods 

  public void setPalabra( String palabra ) {
    this.palabra = palabra;
  }

  public void setSignificado( String significado ) {
    this.significado = significado;
  }
}