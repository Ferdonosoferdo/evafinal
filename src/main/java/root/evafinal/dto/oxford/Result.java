/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evafinal.dto.oxford;

import java.util.List;

/**
 *
 * @author CCRUCES
 */
public class Result {
    private String id;
    private List<LexicalEntries> lexicalEntries;
    private String type;
    private String word;
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

   
    /**
     * @return the lexicalEntries
     */
    public List<LexicalEntries> getLexicalEntries() {
        return lexicalEntries;
    }

    /**
     * @param lexicalEntries the lexicalEntries to set
     */
    public void setLexicalEntries(List<LexicalEntries> lexicalEntries) {
        this.lexicalEntries = lexicalEntries;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }



}
