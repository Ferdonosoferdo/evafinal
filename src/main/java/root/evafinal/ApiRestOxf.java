/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evafinal;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evafinal.dto.PalabraDTO;
import root.evafinal.dto.oxford.Palabra;



/**
 *
 * @author gmesatica
 */

@Path("definicion")
public class ApiRestOxf {
    
    
       //creamos nuestro listar usuario
    @GET
    @Path("/{palabraClave}")
    @Produces(MediaType.APPLICATION_JSON)
   public Response ConsultarDefinicion(@PathParam("palabraClave") String palabraClave){
   
    PalabraDTO palabradto = new PalabraDTO();
     palabradto.setPalabra(palabraClave);
   // palabradto.setSignificado("significado palabra");
     
     Client cliente =ClientBuilder.newClient();
     WebTarget recurso=cliente.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/"+palabraClave);
     Palabra palabra= recurso.request(MediaType.APPLICATION_JSON).header("app_id","97de2f9d").header("app_key","9c2b7495ba21164a142bf4630ffe2005").get(Palabra.class);
     
     String definicion=palabra.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
     //palabradto.setSignificado("significado palabra"+definicion);
     
        palabradto.setSignificado(definicion);
       return Response.ok("200").entity(palabradto).build();
   }
    
}
