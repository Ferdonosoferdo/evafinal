/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evafinal;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evafinal.dao.DiccionarioJpaController;
import root.evafinal.entity.Diccionario;

/**
 *
 * @author gmesatica
 */
@Path("lista")
public class lista {
      @GET
    @Produces(MediaType.APPLICATION_JSON)
   public Response listUsuarios(){
   
       DiccionarioJpaController dao = new DiccionarioJpaController();
       
       List<Diccionario> lista= dao.findDiccionarioEntities();
       return Response.ok("200").entity(lista).build();
   } 
}
