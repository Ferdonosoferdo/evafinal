/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evafinal.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import root.evafinal.dao.DiccionarioJpaController;

import root.evafinal.dto.PalabraDTO;
import root.evafinal.entity.Diccionario;


/**
 *
 * @author gmesatica
 */
@WebServlet(name = "DiccionarioController", urlPatterns = {"/DiccionarioController"})
public class DiccionarioController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DiccionarioController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DiccionarioController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DiccionarioJpaController dao = new DiccionarioJpaController();
        String boton = request.getParameter("buscar");
        String palabra = request.getParameter("palabra");
        String fecha = request.getParameter("fecha");

        // PalabraDTO dto = new PalabraDTO();
        //dto.setPalabra(palabra);
        //Palabra pal = new Palabra();
        Client client = ClientBuilder.newClient();

        //  WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:433/api/v2/entries/es/"+palabra);   
//      http://nb-aogsupport.cemin.com:8080/evafinal-1.0-SNAPSHOT/api/definicion/{palabraClave}
        WebTarget myResource = client.target("https://evafinalfermindonoso.herokuapp.com/api/definicion/" + palabra);
        PalabraDTO palabraDto = myResource.request(MediaType.APPLICATION_JSON).get(PalabraDTO.class);
//     
        palabraDto.setFecha(fecha);

        Diccionario dicc = new Diccionario();
        dicc.setPalabra(palabra);
        dicc.setFecha(fecha);

        try {

            dao.create(dicc);

        } catch (Exception ex) {
            Logger.getLogger(DiccionarioController.class.getName()).log(Level.SEVERE, null, ex);
        }

        request.setAttribute("palabraDto", palabraDto);
        request.getRequestDispatcher("resultado.jsp").forward(request, response);

        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
