<%-- 
    Document   : index
    Created on : 20-10-2021, 22:59:40
    Author     : gmesatica
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="estilos.css" >

    </head>
    <body>

        <section id="seccioninicio" >
            <div class="container text-center">
                <h1>Diccionario de palabras en español, por Oxford</h1>      
                <p>Ingresa la palabra que deseas buscar. </p>
            </div>
            <div class="container-fluid bg-3 text-center">   
                <form id="frm" name="form" action="DiccionarioController" method="POST">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Palabra</th>
                                <th>Fecha</th>   
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" name="palabra" class="form-control" id="palabra" placeholder="palabra"  
                                           ></td>
                                <td><input type="text" name="fecha" class="form-control" id="fecha" placeholder="fecha"  
                                           ></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input type="submit" name="buscar"  value="consultar">  
                    <br>

                    <a class="btn btn-outline-danger" href="https://evafinalfermindonoso.herokuapp.com/api/lista">Listar</a>

                </form>

                <footer id="footer">
                    <p id="txtfooter">Diccionario por Fermín Donoso
                        <br>
                        Taller de Aplicaciones
                        <br>
                        Empresariales
                        <br>
                        Seccion 51,
                        2021
                    </p>
                <p>git clone https://Ferdonosoferdo@bitbucket.org/Ferdonosoferdo/evafinal.git </p>
                </footer>

                </body>
                </html>
